/* eslint-disable import/no-extraneous-dependencies, import/no-unresolved, import/extensions */
import React from 'react'
import { configure, addDecorator } from '@storybook/react'
import { muiTheme } from 'storybook-addon-material-ui'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { BrowserRouter as Router } from 'react-router-dom'

import reducers from '../src/reducers'

const store = createStore(
  reducers,
  applyMiddleware(thunkMiddleware)
)

addDecorator(muiTheme())

addDecorator((getStory) => (
  <Provider store={store} >
    <Router>
      { getStory() }
    </Router>
  </Provider>
))

const req = require.context('../src/stories/', true, /.js$/)

const loadStories = () => {
  req.keys().forEach(req)
}

configure(loadStories, module)

