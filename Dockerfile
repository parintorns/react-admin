FROM node:7.10.0-alpine
RUN mkdir $HOME/src
ADD . $HOME/src
WORKDIR $HOME/src
RUN npm install && npm run build
CMD ["node", "server"]
EXPOSE 8000

