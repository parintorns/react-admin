const Firebase = {
  auth: () => ({
    onAuthStateChanged: (f, callback) => {
      f({
        uid: 'user-id',
        photoURL: 'http://photo.com/url.jpg',
        email: 'mail@email.com',
        emailVerified: true,
        isAnonymous: false,
        providerData: [{
          providerId: 'provider-id'
        }]
      })
      f({
        uid: 'user-id',
        displayName: 'Display Name',
        photoURL: 'http://photo.com/url.jpg',
        email: 'mail@email.com',
        emailVerified: true,
        isAnonymous: false,
        providerData: [{
          providerId: 'provider-id'
        }]
      })
      f(false)
      callback('error')
    },
    getRedirectResult: (f) => {
      return Promise.resolve({uid: 'asdfasdf'})
    },
    signOut: () =>
      Promise.resolve({}),
    createUserWithEmailAndPassword: (email, password) =>
      email === 'error'
        ? Promise.reject({ code: 'auth/email-already-exists', message: 'Email already exists' })
        : Promise.resolve({
          uid: '123',
          email: 'test@test.com',
          providerData: [{}],
          updateProfile: profile =>
            profile.displayName === 'Display Name'
              ? Promise.resolve(profile)
              : Promise.reject({ code: 'update-error'})
        }),
    signInWithCustomToken: () => {
      return Promise.resolve({
        toJSON: () => ({
          stsTokenManager: {
            accessToken: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJjbGFpbXMiOnsiZGlzcGxheU5hbWUiOiJFZHdhcmQgV3UiLCJlbWFpbCI6ImVkZGlld3U4MEBnbWFpbC5jb20iLCJvcmlnaW5hbElkIjoiSlFYQ2dRTkxEU1lSMFEzdjlwY3FjTDZJeGRUMiIsInByb3ZpZGVyRGF0YSI6W3siZGlzcGxheU5hbWUiOiJFZHdhcmQgV3UiLCJlbWFpbCI6ImVkZGlld3U4MEBnbWFpbC5jb20ifV19LCJ1aWQiOiJqaTh3c1BDVW5PYUhvUGZBalNCS2ZReU1pTmkxIiwiaWF0IjoxNDc1NDM3MDMyLCJleHAiOjE0NzU0NDA2MzIsImF1ZCI6Imh0dHBzOi8vaWRlbnRpdHl0b29sa2l0Lmdvb2dsZWFwaXMuY29tL2dvb2dsZS5pZGVudGl0eS5pZGVudGl0eXRvb2xraXQudjEuSWRlbnRpdHlUb29sa2l0IiwiaXNzIjoicmVzaWRlLXByb2RAcmVzaWRlLXByb2QuaWFtLmdzZXJ2aWNlYWNjb3VudC5jb20iLCJzdWIiOiJyZXNpZGUtcHJvZEByZXNpZGUtcHJvZC5pYW0uZ3NlcnZpY2VhY2NvdW50LmNvbSJ9.aOXOCCAL-lI5AVnd8MVlc86exvCGNySq8X7DM4Gr7PG0ek5mh_8qnFfLuzw2gfv6mHNVgY2UngUmG0qETaBdox7l3wBo1GdP9hB1bM8NltCYffXwxyw7sN36BFWD3l-cz4rlxfmfzosCLj3XtDK8ARDQ76pAXxsK-rRBvMG6N-wgE_ZLf17FVvwB95e1DAmL39fp6dRVxoPflG--m4QEKVk8xIeDx4ol9HJw512gMGtTkRDMEPWVJEdaEAp6L6Lo2-Bk-TxBCHs8gpb7b7eidWMUEXObk0UPQIz2DRh-3olbruimzL_SgPNg4Pz0uUYSn11-Mx_HxxiVtyQj1ufoLA'
          },
          uid: 'asdfasdfsdf'
        })
      })
    },
    signInWithEmailAndPassword: (email, password) =>
      email === 'error2'
        ? Promise.reject({ code: 'auth/not-login', message: 'Can\'t login' })
        : email === 'error3'
          ? Promise.reject({ code: 'auth/user-not-found', message: 'User not found' })
          : Promise.resolve({ uid: '123', email: 'test@test.com', providerData: [{}] }),
    signInWithPopup: (provider) =>
      !provider
        ? Promise.reject({ code: 'auth/user-not-found', message: 'User not found' })
        : Promise.resolve({ uid: '123', email: 'test@test.com', providerData: [{}] }),
    sendPasswordResetEmail: (email) =>
      email === 'invalidemail'
      ? Promise.reject({ code: 'auth/invalid-email', message: 'The email address is badly formatted.' })
      : email === 'notfound@mail.com'
        ? Promise.reject({ code: 'auth/user-not-found', message: 'There is no user record corresponding to this identifier. The user may have been deleted.' })
        : Promise.resolve({ some: 'val' }),
    confirmPasswordReset: (code, password) =>
      password === 'error'
        ? Promise.reject({code: code})
        : Promise.resolve(),
    verifyPasswordResetCode: (code) =>
      code === 'error'
        ? Promise.reject({ code: 'some' })
        : Promise.resolve('success'),
    currentUser: {
      updateProfile: profile =>
        profile.displayName !== 'Good Profile'
          ? Promise.reject({ code: 'update-error'})
          : Promise.resolve(profile),
      updateEmail: email =>
        email !== 'new@email.com'
          ? Promise.reject({ code: 'update-error'})
          : Promise.resolve(email)
    }
  })
}

const FirebaseAuth = {
  FacebookAuthProvider: () => {providerId: 'facebook.com'},
  GoogleAuthProvider: () => {providerId: 'google.com'}
}

export { Firebase, FirebaseAuth }
