import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import {
  SignInWithEmailAndPassword,
  SignInWithPopup,
  CreateUserWithEmailAndPassword,
  SignOut,
  SendPasswordResetEmail,
  GetUserProfile,
  UpdateProfile,
  UpdateUserEmail
} from '../../src/actions'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

jest.mock('../../src/config/firebase', () => require('../../__mocks__/firebase'))

let store

beforeEach(() => {
  store = mockStore()
})

afterEach(() => {
  store.clearActions()
})

describe('Login/Logout Actions', () => {
  it('should login with email and password', async () => {
    const expectdAction = []

    await store.dispatch(SignInWithEmailAndPassword('facebook'))
    expect(store.getActions()).toEqual(expectdAction)
  })

  it('should login with facebook account', async () => {
    const expectdAction = []

    await store.dispatch(SignInWithPopup('facebook'))
    expect(store.getActions()).toEqual(expectdAction)
  })

  it('should login with google account', async () => {
    const expectdAction = []

    await store.dispatch(SignInWithPopup('google'))
    expect(store.getActions()).toEqual(expectdAction)
  })

  it('should login without provider', async () => {
    const expectdAction = []

    await store.dispatch(SignInWithPopup(''))
    expect(store.getActions()).toEqual(expectdAction)
  })

  it('should logging out', async () => {
    const expectdAction = [{
      type: 'CLEAR_PROFILE'
    }]

    await store.dispatch(SignOut())
    expect(store.getActions()).toEqual(expectdAction)
  })
})

describe('Create User', () => {
  it('should create user with email and password', async () => {
    const email = 'email@mail.com'
    const password = 'password'
    const userData = {
      displayName: 'Display Name'
    }
    const expectdAction = [{
      type: 'UPDATE_PROFILE',
      payload: userData
    }]

    await store.dispatch(CreateUserWithEmailAndPassword(email, password, userData))
    expect(store.getActions()).toEqual(expectdAction)
  })
})

describe('Get User Profile', () => {
  it('should get user profile', async () => {
    const userData = {
      uid: 'user-id',
      photoURL: 'http://photo.com/url.jpg',
      email: 'mail@email.com',
      emailVerified: true,
      isAnonymous: false,
      providerId: 'provider-id'
    }
    const userDataWithDisplayName = {
      uid: 'user-id',
      displayName: 'Display Name',
      photoURL: 'http://photo.com/url.jpg',
      email: 'mail@email.com',
      emailVerified: true,
      isAnonymous: false,
      providerId: 'provider-id'
    }
    const expectdAction = [{
      type: 'GET_USER_PROFILE',
      payload: userData
    }, {
      type: 'GET_USER_PROFILE',
      payload: userDataWithDisplayName
    }]

    await store.dispatch(GetUserProfile())
    expect(store.getActions()).toEqual(expectdAction)
  })
})

describe('Update Profile', () => {
  it('should update profile', async () => {
    const user = {
      displayName: 'Good Profile',
      photoURL: 'Photo URL'
    }

    const expectdAction = [{
      type: 'UPDATE_PROFILE',
      payload: user
    }]

    await store.dispatch(UpdateProfile(user))
    expect(store.getActions()).toEqual(expectdAction)
  })

  it('should not update bad profile', async () => {
    const profile = {
      displayName: 'Bad Profile',
      photoURL: 'Photo URL'
    }

    const expectdAction = [{
      type: 'UPDATE_PROFILE_FAILED',
      payload: { code: 'update-error'}
    }]

    await store.dispatch(UpdateProfile(profile))
    expect(store.getActions()).toEqual(expectdAction)
  })

  it('should update email', async () => {
    const email = 'new@email.com'

    const expectdAction = [{
      type: 'UPDATE_PROFILE',
      payload: { email }
    }]

    await store.dispatch(UpdateUserEmail(email))
    expect(store.getActions()).toEqual(expectdAction)
  })

  it('should not update bad email', async () => {
    const email = 'bad@email.com'

    const expectdAction = [{
      type: 'UPDATE_PROFILE_FAILED',
      payload: { code: 'update-error'}
    }]

    await store.dispatch(UpdateUserEmail(email))
    expect(store.getActions()).toEqual(expectdAction)
  })
})

describe('Reset Password', () => {
  it('should send password reset email', async () => {
    const email = 'email'
    const expectdAction = []

    await store.dispatch(SendPasswordResetEmail(email))
    expect(store.getActions()).toEqual(expectdAction)
  })
})
