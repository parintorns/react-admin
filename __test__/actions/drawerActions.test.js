import {
  TOGGLE_DRAWER
} from '../../src/config'

import {
	ToggleDrawer
} from '../../src/actions/drawerActions'

describe('Drawer action', () => {
  it('should create action to toggle drawer', () => {
    const expectdAction = {
      type: TOGGLE_DRAWER
    }

    expect(ToggleDrawer()).toEqual(expectdAction)
  })
})
