import React from 'react'
import { shallow } from 'enzyme'

import { 
  SideMenu,
  mapStateToProps,
  mapDispatchToProps
} from '../../src/components/SideMenu'

const props = {
  drawer: true,
  ToggleDrawer: jest.fn()
}

let wrapper

beforeEach(() => {
  wrapper= shallow(
    <SideMenu
      {...props}
    />
  )
})

describe('Map Props', () => {
  it('should map state tp props', () => {
    expect(mapStateToProps({})).toBeDefined()
  })

  it('should map dispatch tp props', () => {
    expect(mapDispatchToProps({})).toBeDefined()
  })
})

describe('SideMenu', () => {
  it('Render SideMenu mobile mode', () => {
    wrapper.setProps({
      isMobile: true
    })
    expect(wrapper).toMatchSnapshot()

    wrapper.setProps({
      drawer: false
    })
    expect(wrapper).toMatchSnapshot()
  })

  it('Render SideMenu desktop mode', () => {
    wrapper.setProps({
      isMobile: false
    })
    expect(wrapper).toMatchSnapshot()

    wrapper.setProps({
      drawer: false
    })
    expect(wrapper).toMatchSnapshot()
  })

  it('Close Drawer on Touch Tap Mobile Mode', () => {
    wrapper.setProps({
      isMobile: true
    })

    const drawer = wrapper.find('Drawer')
    drawer.simulate('requestChange')
    
    const listItems = wrapper.find('ListItem')
    listItems.map(item => {
      item.simulate('touchTap')
    })
    expect(props.ToggleDrawer.mock.calls.length).toEqual(3)
  })
})
