import React from 'react'
import { shallow } from 'enzyme'

import Snackbar from 'material-ui/Snackbar'

import { SocialLogin } from '../../src/components/SocialLogin'

jest.mock('../../src/config/firebase', () => require('../../__mocks__/firebase'))

const spyCloseSnackBar = jest.spyOn(SocialLogin.prototype, 'closeSnackBar')
const spyLoginWith = jest.spyOn(SocialLogin.prototype, 'loginWith')

const props = {
  SignInWithPopup: jest.fn(providerId =>
    providerId !== 'facebook' && providerId !== 'google'
      ? Promise.reject({ code: 'auth/not-login', message: 'Can\'t login' })
      : Promise.resolve(true)
  )
}

let wrapper

beforeEach(() => {
  wrapper = shallow(
    <SocialLogin
      {...props}
    />
  )
})

afterEach(() => {
  jest.clearAllMocks()
})

describe('Rendering', () => {
  it('should render button successful', () => {
    expect(wrapper).toMatchSnapshot()
  })
})

describe('Render Snackbar', () => {
  it('should open snackbar', () => {
    wrapper.setState({
      signInError: {
        message: 'Snackbar Mesasge'
      }
    })
    expect(wrapper).toMatchSnapshot()
  })

  it('should close snackbar', () => {
    wrapper.setState({
      signInError: {
        message: 'Snackbar Mesasge'
      }
    })

    const snackbar = wrapper.find(Snackbar)
    expect(snackbar).toHaveLength(1)

    snackbar.simulate('requestClose')
    expect(spyCloseSnackBar).toHaveBeenCalledTimes(1)
  })
})

describe('Actions', () => {
  it('should login with Facebook', async () => {
    const btnFacebook = wrapper.find({ id: 'btnFacebook' })
    expect(btnFacebook).toHaveLength(1)

    btnFacebook.simulate('click')
    expect(spyLoginWith).toHaveBeenCalledTimes(1)

    await wrapper.instance().loginWith('facebook')
    await wrapper.update()

    expect(wrapper.state('signInError')).toEqual(false)
  })

  it('should login with Google', async () => {
    const btnGoogle = wrapper.find({ id: 'btnGoogle' })
    expect(btnGoogle).toHaveLength(1)

    btnGoogle.simulate('click')
    expect(spyLoginWith).toHaveBeenCalledTimes(1)

    await wrapper.instance().loginWith('google')
    await wrapper.update()

    expect(wrapper.state('signInError')).toEqual(false)
  })

  it('should not login with social account', async () => {
    const expected = {
      code: 'auth/not-login', message: 'Can\'t login'
    }

    await wrapper.instance().loginWith('bad-provider')
    await wrapper.update()

    expect(wrapper.state('signInError')).toEqual(expected)
  })
})
