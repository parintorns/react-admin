import React from 'react'
import { shallow } from 'enzyme'
import Dialog from 'material-ui/Dialog'
import MenuItem from 'material-ui/MenuItem'

import { 
  TopMenu, 
  mapStateToProps,
  mapDispatchToProps
} from '../../src/components/TopMenu'

jest.mock('../../src/config/firebase', () => require('../../__mocks__/firebase'))

const spyHandleOpen = jest.spyOn(TopMenu.prototype, 'handleOpen')

const props = {
  account: {
    displayName: "Display Name",
    email: "email@mail.com",
    photoURL: "Photo URL"
  },
  ClearProfile: jest.fn(),
  ToggleDrawer: jest.fn(),
  SignOut: jest.fn()
}

let wrapper

beforeEach(() => {
  wrapper = shallow(
    <TopMenu
      {...props}
    />
  )
})

describe('Map Props', () => {
  it('should map state tp props', () => {
    expect(mapStateToProps({})).toBeDefined()
  })

  it('should map dispatch tp props', () => {
    expect(mapDispatchToProps({})).toBeDefined()
  })
})

describe('Render', () => {
  it('should render TopMenu', () => {
    expect(wrapper).toMatchSnapshot()
  })
  it('should open logout dialog', () => {
    wrapper.setState({ openDialog: true })
    const dialog = wrapper.find(Dialog)

    expect(wrapper).toMatchSnapshot()
  })

  it('should close logout dialog', () => {
    wrapper.setState({ openDialog: false })
    const dialog = wrapper.find(Dialog)

    expect(wrapper).toMatchSnapshot()
  })
})

describe('Actions', () => {
  it('Toggle Drawer', () => {
    const appBarIcon = wrapper.find('AppBar')

    appBarIcon.simulate('leftIconButtonClick')
    expect(props.ToggleDrawer.mock.calls.length).toEqual(1)
  })

  it('Logout Dialog', () => {
    wrapper.instance().handleOpen()
    expect(wrapper.state('openDialog')).toBe(true)

    wrapper.instance().handleClose()
    expect(wrapper.state('openDialog')).toBe(false)
  })

  it('User Logout', async () => {
    await wrapper.instance().logout()

    expect(props.SignOut.mock.calls.length).toEqual(1)
    expect(wrapper.state('openDialog')).toBe(false)
  })
})
