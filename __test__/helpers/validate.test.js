import {required, letter, email} from '../../src/helpers/validate'

describe('Validation helpers', () => {
  it('required', () => {
    expect(required('any')).toEqual(undefined)
    expect(required()).toEqual("Required")
  })

  it('letter only', () => {
    expect(letter('abcabc')).toEqual(undefined)
    expect(letter('abc-cde')).toEqual(undefined)
    expect(letter('Firstname Lastname')).toEqual(undefined)
    expect(letter('o\' conner')).toEqual(undefined)
    expect(letter('abc123')).toEqual("Letter Only")
    expect(letter('abc,abc')).toEqual("Letter Only")
  })

  it('correct email format', () => {
    expect(email('abc@mail.com')).toEqual(undefined)
    expect(email('abc-abc@mail.com')).toEqual(undefined)
    expect(email('abc123@mail.com')).toEqual(undefined)
    expect(email('abc.adf@mail.com')).toEqual(undefined)
    expect(email('abc.adf@mail.co')).toEqual(undefined)
    expect(email('abc123.mail.c')).toEqual("Invalid email address")
  })
})
