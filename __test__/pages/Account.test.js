import React from 'react'
import { shallow } from 'enzyme'
import Snackbar from 'material-ui/Snackbar'

import { 
  Account,
  mapStateToProps,
  mapDispatchToProps
} from '../../src/pages/Account'

jest.mock('../../src/config/firebase', () => require('../../__mocks__/firebase'))

const props = {
	handleSubmit: jest.fn(),
	pristine: true,
	submitting: true,
	initialValues: {},
	UpdateProfile: jest.fn(),
	UpdateUserEmail: jest.fn()
}

let wrapper

beforeEach(() => {
  wrapper = shallow(
    <Account {...props} />
  )
})

describe('Map Props', () => {
  it('should map state tp props', () => {
    expect(mapStateToProps({})).toBeDefined()
  })

  it('should map dispatch tp props', () => {
    expect(mapDispatchToProps({})).toBeDefined()
  })
})

describe('Render Account Page', () => {
  it('should render with data', () => {
  	expect(wrapper).toMatchSnapshot()
  })

  it('should render with update button states', () => {
  	wrapper.setProps({
  		pristine: false
  	})

  	expect(wrapper).toMatchSnapshot()
  })
})

describe('Actions', () => {
  it('should close snackbar', () => {
    wrapper.setState({ 
      updateError: {
        message: 'Some Error'
      }
    })

    const snackbar = wrapper.find(Snackbar)
    expect(snackbar).toHaveLength(1)

    snackbar.simulate('requestClose')
    expect(wrapper.state('updateError')).toEqual(false)
  })
})

