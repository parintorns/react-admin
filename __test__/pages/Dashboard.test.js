import React from 'react'
import { shallow } from 'enzyme'

import { Dashboard } from '../../src/pages/Dashboard'

let wrapper

beforeEach(() => {
  wrapper = shallow(
    <Dashboard />
  )
})

describe('Render Dashboard Page', () => {
  it('should render with data', () => {
  	expect(wrapper).toMatchSnapshot()
  })
})

