import React from 'react';
import { shallow } from 'enzyme';

import { 
  Login,
  mapDispatchToProps
} from '../../src/pages/Login'

jest.mock('../../src/config/firebase', () => require('../../__mocks__/firebase'))

let wrapper;

const props = {
  handleSubmit: jest.fn(),
  SignInWithEmailAndPassword: jest.fn(email =>
    email === 'error2'
      ? Promise.reject({ code: 'auth/not-login', message: 'Can\'t login' })
      : email === 'error3'
        ? Promise.reject({ code: 'auth/user-not-found', message: 'User not found' })
        : Promise.resolve({ uid: '123', email: 'test@test.com', providerData: [{}] })
  ),
  SignInWithPopup: jest.fn(() => Promise.resolve(true))
}

beforeEach(() => {
  wrapper = shallow(
    <Login
      {...props}
    />
  );
})

describe('Map Props', () => {
  it('should map dispatch tp props', () => {
    expect(mapDispatchToProps({})).toBeDefined()
  })
})

describe('Login button', () => {
  it('should render login form correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })

	it('should disabled with empty inputs', () => {
    expect(wrapper.find({label: "Login"}).getElement().props.disabled).toEqual(true)
	})

	it('should disabled with input only email', () => {
    wrapper.setState({
      email: "email"
    })

    expect(wrapper.find({label: "Login"}).getElement().props.disabled).toEqual(true)
  })

  it('should disabled with input only password', () => {
    wrapper.setState({
      password: "password"
    })

    expect(wrapper.find({label: "Login"}).getElement().props.disabled).toEqual(true)
	})

	it('should enabled with all input entered', () => {
    wrapper.setState({
      email: "email",
      password: "password"
    })

    expect(wrapper.find({label: "Login"}).getElement().props.disabled).toEqual(false)
	})
})

describe('Input Login Form', () => {
  const email = {
    target: {
      value: 'email'
    }
  }

  const password = {
    target: {
      value: 'password'
    }
  }

  it('input email', () => {
    wrapper.find('Field').get(0).props.onChange(email)

    expect(wrapper.state('email')).toEqual('email')
  })

  it('input password', () => {
    wrapper.find('Field').get(1).props.onChange(password)

    expect(wrapper.state('password')).toEqual('password')
  })
})

describe('User Login', () => {
  it('with correct email', async () => {
    await wrapper.instance().login({
      email: 'correct@mail.com',
      password: 'correct'
    })

    await wrapper.update()

    expect(wrapper.state('signInError')).toEqual(false)
  })

  it('with incorrect email', async () => {
    const expected = {
      code: 'auth/not-login',
      message: 'Can\'t login'
    }

    await wrapper.instance().login({
      email: 'error2',
      password: 'anything'
    })

    await wrapper.update()

    expect(wrapper.state('signInError')).toEqual(expected)
  })

  it('with not found email', async () => {
    const expected = {
      code: 'auth/user-not-found',
      message: 'User not found'
    }

    await wrapper.instance().login({
      email: 'error3',
      password: 'anything'
    })

    await wrapper.update()

    expect(wrapper.state('signInError')).toEqual(expected)
  })

  it('should close snackbar', () => {
    wrapper.instance().closeSnackBar()

    expect(wrapper.state('signInError')).toEqual(false)
  })
})
