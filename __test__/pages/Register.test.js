import React from 'react'
import { shallow } from 'enzyme'

import { 
  Register,
  mapDispatchToProps
} from '../../src/pages/Register'

jest.mock('../../src/config/firebase', () => require('../../__mocks__/firebase'))

const props = {
  handleSubmit: jest.fn(),
  SignInWithPopup: jest.fn(() => Promise.resolve(true)),
  CreateUserWithEmailAndPassword: jest.fn((email, password, userData) =>
    email === 'error'
      ? Promise.reject({ code: 'auth/email-already-exists', message: 'Email already exists' })
      : Promise.resolve({
        uid: '123',
        email: 'test@test.com',
        providerData: [{}],
        updateProfile: profile =>
          profile.displayName === 'Display Name'
            ? Promise.resolve(profile)
            : Promise.reject({ code: 'update-error'})
      })
  )
}

let wrapper

beforeEach(() => {
  wrapper = shallow(
    <Register
      {...props}
    />
  )
})

describe('Map Props', () => {
  it('should map dispatch tp props', () => {
    expect(mapDispatchToProps({})).toBeDefined()
  })
})

describe('Register button', () => {

  it('should render login form correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })

	it('should disabled with empty inputs', () => {
    expect(wrapper.find({id: "btnRegister"}).getElement().props.disabled).toBe(true)
  })

  it('should disabled with input only username', () => {
    const inputDisplayName = wrapper.find({ id: 'inputDisplayName' })

    inputDisplayName.simulate('change', { target: { value: 'Display Name' } })

    expect(wrapper.find({id: "btnRegister"}).getElement().props.disabled).toBe(true)
  })

  it('should disabled with input only email', () => {
    const inputEmail = wrapper.find({ id: 'inputEmail' })

    inputEmail.simulate('change', { target: { value: 'email@mail.com' } })

    expect(wrapper.find({id: "btnRegister"}).getElement().props.disabled).toBe(true)
  })

  it('should disabled with input only password', () => {
    const inputPassword = wrapper.find({ id: 'inputPassword' })

    inputPassword.simulate('change', { target: { value: 'password' } })

    expect(wrapper.find({id: "btnRegister"}).getElement().props.disabled).toBe(true)
  })

  it('should enabled with all input entered', () => {
    const inputDisplayName = wrapper.find({ id: 'inputDisplayName' })
    const inputEmail = wrapper.find({ id: 'inputEmail' })
    const inputPassword = wrapper.find({ id: 'inputPassword' })

    inputDisplayName.simulate('change', { target: { value: 'Display Name' } })
    inputEmail.simulate('change', { target: { value: 'email@mail.com' } })
    inputPassword.simulate('change', { target: { value: 'password' } })

    expect(wrapper.find({id: "btnRegister"}).getElement().props.disabled).toBe(false)
	})
})

describe('User Register', () => {
  it('with successful email', async () => {
    await wrapper.instance().handleLoginWithEmail({
      email: 'success@mail.com',
      password: 'password',
      displayName: 'Display Name'
    })

    await wrapper.update()

    expect(wrapper.state('signUpError')).toEqual(false)
  })

  it('with email already exists', async () => {
    const expected = {
      code: 'auth/email-already-exists',
      message: 'Email already exists'
    }

    await wrapper.instance().handleLoginWithEmail({
      email: 'error',
      password: 'password',
      displayName: 'Display Name'
    })

    await wrapper.update()

    expect(wrapper.state('signUpError')).toEqual(expected)
  })

  it('should close snackbar', async () => {
    wrapper.instance().closeSnackBar()

    await wrapper.update()

    expect(wrapper.state('signUpError')).toBe(false)
  })
})
