import React from 'react'
import { shallow } from 'enzyme'
import Snackbar from 'material-ui/Snackbar'

import { 
  ResetPassword,
  mapDispatchToProps
} from '../../src/pages/ResetPassword'

jest.mock('../../src/config/firebase', () => require('../../__mocks__/firebase'))

const props = {
  SendPasswordResetEmail: jest.fn(email =>
    email === 'validemail'  
      ? Promise.resolve(true)
      : Promise.reject({
          message: 'Email not valid'
        })
    ),
  handleSubmit: jest.fn()
}

let wrapper

const mocks = {
  invalidEmail: {
    target: {
      value: 'invalidemail'
    }
  },
  validButNotFoundEmail: {
    target: {
      value: 'notfound@mail.com'
    }
  }
}

beforeEach(() => {
  wrapper = shallow(
    <ResetPassword
      {...props}
    />
  )
})

afterEach(() => {
  jest.clearAllMocks()
})

describe('Map Props', () => {
  it('should map dispatch tp props', () => {
    expect(mapDispatchToProps({})).toBeDefined()
  })
})

describe('Reset Password Form', () => {
  it('should render Reset Password form correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })

  it('should disabled submit button if empty email input', () => {
    expect(wrapper.find({id: "btnSubmit"}).getElement().props.disabled).toBe(true)
  })

  it('should enabled submit button if email input', () => {
    wrapper.find({id: 'inputEmail'})
      .simulate('change', mocks.invalidEmail)

    expect(wrapper.find({id: "btnSubmit"}).getElement().props.disabled).toBe(false)
  })
})

describe('Reset Password Request', () => {
  it('should not send email if invalid email', async () => {
    await wrapper.instance().resetPassword({
      email: 'invalidemail'
    })

    expect(wrapper.state('resetPasswordError')).toEqual(false)
  })

  it('should send password reset email', async () => {
    const expected = {
      message: 'Please check your email'
    }

    await wrapper.instance().resetPassword({
      email: 'validemail'
    })

    expect(wrapper.state('resetPasswordError')).toEqual(expected)
  })

  it('should close snackbar', () => {
    wrapper.setState({ 
      resetPasswordError: {
        message: 'Some Error'
      }
    })

    const snackbar = wrapper.find(Snackbar)
    expect(snackbar).toHaveLength(1)

    snackbar.simulate('requestClose')
    expect(wrapper.state('resetPasswordError')).toEqual(false)
  })
})

