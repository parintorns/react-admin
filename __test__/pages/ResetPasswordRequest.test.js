import React from 'react'
import { shallow } from 'enzyme'

import { ResetPasswordRequest } from '../../src/pages/ResetPasswordRequest'

let wrapper

beforeEach(() => {
  wrapper = shallow(
    <ResetPasswordRequest />
  )
})

describe('Reset Password Form', () => {
	it('should render request email page successful', () => {
		expect(wrapper).toMatchSnapshot()
	})
})
