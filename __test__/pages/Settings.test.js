import React from 'react'
import { shallow } from 'enzyme'

import { Settings } from '../../src/pages/Settings'

let wrapper

beforeEach(() => {
  wrapper = shallow(
    <Settings />
  )
})

describe('Render Settings Page', () => {
  it('should render with data', () => {
  	expect(wrapper).toMatchSnapshot()
  })
})

