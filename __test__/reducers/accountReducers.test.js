import reducer from '../../src/reducers/accountReducers'

describe('Account reducers', () => {

  it('should return the empty state', () => {
    const action = {
      type: "ANYTHING"
    }
    expect(reducer(undefined, action)).toEqual({})
  })

  it('should return the initial state', () => {
    const state = {
      initial: 'state'
    }
    const action = {
      type: "ANYTHING"
    }
    expect(reducer(state, action)).toEqual(state)
  })

  it('should clear profile', () => {
    const action = {
      type: 'CLEAR_PROFILE'
    }

    expect(reducer({}, action)).toEqual(null)
  })

  it('should get user profile', () => {
    const user = {
      displayName: "Display Name",
      email: "email@mail.com",
      photoURL: "Photo URL"
    }

    const action = {
      type: 'GET_USER_PROFILE',
      payload: user
    }

    expect(reducer(user, action)).toEqual(user)
  })

  it('should update profile', () => {

    const user = {
      displayName: "Old Display Name",
      email: "old_email@mail.com",
      photoURL: "Old Photo URL"
    }

    const updateUser = {
      displayName: "Updated Display Name",
      email: "updated_email@mail.com",
      photoURL: "Updated Photo URL"
    }

    const action = {
      type: 'UPDATE_PROFILE',
      payload: updateUser
    }

    expect(reducer(user, action)).toEqual(updateUser)
  })
})

