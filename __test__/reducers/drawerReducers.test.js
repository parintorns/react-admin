import reducer from '../../src/reducers/drawerReducers'

describe('Drawer reducer', () => {

  it('should return the empty state', () => {
    const action = {
      type: "ANYTHING"
    }
    expect(reducer(undefined, action)).toEqual(true)
  })

  it('should return the initial state', () => {
    const state = {
      initial: 'state'
    }
    const action = {
      type: "ANYTHING"
    }
    expect(reducer(state, action)).toEqual(state)
  })

  const drawer = false

  it('should toggle drawer', () => {
    const action = {
      type: 'TOGGLE_DRAWER'
    }

    expect(reducer(drawer, action)).toEqual(!drawer)
    expect(reducer(!drawer, action)).toEqual(drawer)
  })
})
