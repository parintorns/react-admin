Feature: Authentication User
  In order access to system
  As a User
  I want to control my account

  Scenario: Reset Password Request
    Given I am on "Reset Password" Page
     When I put my email into "email" text box
      And email format correct
      And "Reset Password" button is clickable
      And I click "Reset Password" button
     Then I am going to "Check Email" Page

  Scenario: Set New Password
    Given I am on "Set New Password" Page
     When I input my new password
      And My password length more than 8 characters
      And I click "Set New Password" button
     Then I see pop-up to tell me it's success
      And I am going to "Dashboard" Page















