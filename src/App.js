import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import CircularProgress from 'material-ui/CircularProgress'
import { isEmpty } from 'underscore'
import injectTapEventPlugin from 'react-tap-event-plugin'

import Login from './pages/Login'
import Register from './pages/Register'
import ResetPassword from './pages/ResetPassword'
import SideMenu from './components/SideMenu'
import TopMenu from './components/TopMenu'
import {
  GetUserProfile,
  UpdateProfile
} from './actions'
import styles from './styles.js'

import Account from './pages/Account'
import Dashboard from './pages/Dashboard'
import Settings from './pages/Settings'

injectTapEventPlugin()

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loaded: false
    }
  }

  async componentWillMount() {
    await this.props.GetUserProfile()
    this.setState({
      loaded: true
    })
  }

  render() {
    const { props } = this
    const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent)

    if (this.state.loaded) {
      if (isEmpty(props.account)) {
        return (
          <Router>
            <Switch>
              <Route exact path="/" component={Login} />
              <Route path="/register" component={Register} />
              <Route path="/reset-password" component={ResetPassword} />
              <Redirect from="*" to="/" />
            </Switch>
          </Router>
        )
      } else {
        return (
          <Router>
            <div style={styles.main}>

              {/* TopMenu */}
              <TopMenu />
              {/* TopMenu */}

              <div style={styles.body}>

                {/* Side Menu*/}
                <SideMenu isMobile={isMobile} />
                {/* Side Menu*/}

                {/* Contents */}
                <div style={styles.content}>
                  <Switch>
                    <Route exact path="/" component={Dashboard} />
                    <Route path="/dashboard" component={Dashboard} />
                    <Route path="/account" component={Account} />
                    <Route path="/settings" component={Settings} />
                    <Redirect from="*" to="/" />
                  </Switch>
                </div>
                {/* Contents */}

              </div>
            </div>
          </Router>
        )
      }
    } else {
      return (
        <div style={styles.loading}>
          <CircularProgress />
        </div>
      )
    }
  }
}

App.propTypes = {
  account: PropTypes.object,
  GetUserProfile: PropTypes.func.isRequired,
  UpdateProfile: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  account: state.account
})

const mapDispatchToProps = dispatch => {
  /* istanbul ignore next */
  return bindActionCreators({
    GetUserProfile: GetUserProfile,
    UpdateProfile: UpdateProfile
  }, dispatch)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
