import {
  GET_USER_PROFILE,
  UPDATE_PROFILE,
  UPDATE_PROFILE_FAILED,
  CLEAR_PROFILE,
} from '../config'

import {
  Firebase,
  FirebaseAuth
} from '../config/firebase'

export const SignInWithEmailAndPassword = (email, password) => {
  return () => {
    return Firebase.auth().signInWithEmailAndPassword(email, password)
  }
}

export const SignInWithPopup = providerId => {
  let provider

  return () => {
    switch (providerId) {
      case 'facebook': {
        provider = new FirebaseAuth.FacebookAuthProvider()
        return Firebase.auth().signInWithPopup(provider)
      }

      case 'google': {
        provider = new FirebaseAuth.GoogleAuthProvider()
        return Firebase.auth().signInWithPopup(provider)
      }

      default: return false
    }
  }
}

export const CreateUserWithEmailAndPassword = (email, password, userData) => {
  return dispatch => {
    return Firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(user => {
        user.updateProfile(userData)
      })
      .then(() => {
        dispatch({
          type: UPDATE_PROFILE,
          payload: userData
        })
      })
  }
}

export const SignOut = () => {
  return dispatch => {
    return Firebase.auth().signOut()
      .then(() => {
        dispatch({
          type: CLEAR_PROFILE
        })
      })
  }
}

export const SendPasswordResetEmail = email => {
  return () => {
    return Firebase.auth().sendPasswordResetEmail(email)
  }
}

export const GetUserProfile = () => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      Firebase.auth().onAuthStateChanged(user => {
        if (user) {
          const userData = {
            uid: user.uid,
            photoURL: user.photoURL,
            email: user.email,
            emailVerified: user.emailVerified,
            isAnonymous: user.isAnonymous,
            providerId: user.providerData[0].providerId
          }

          if (user.displayName) userData.displayName = user.displayName

          dispatch({
            type: GET_USER_PROFILE,
            payload: userData
          })

          resolve(user.uid)
        } else {
          resolve('no user')
        }
      }, error =>
        reject(Error(error))
      )
    })
  }
}

export const UpdateProfile = profile => {
  return dispatch => {
    const user = Firebase.auth().currentUser

    return user.updateProfile(profile)
      .then(() => {
        dispatch({
          type: UPDATE_PROFILE,
          payload: profile
        })
      })
      .catch(error => {
        dispatch({
          type: UPDATE_PROFILE_FAILED,
          payload: error
        })
      })
  }
}

export const UpdateUserEmail = email => {
  return dispatch => {
    const user = Firebase.auth().currentUser
    return user.updateEmail(email)
      .then(() => {
        dispatch({
          type: UPDATE_PROFILE,
          payload: { email }
        })
      })
      .catch(error => {
        dispatch({
          type: UPDATE_PROFILE_FAILED,
          payload: error
        })
      })
  }
}
