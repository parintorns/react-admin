import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Paper from 'material-ui/Paper'
import {List, ListItem} from 'material-ui/List'
import Drawer from 'material-ui/Drawer'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Link } from 'react-router-dom'

import { ToggleDrawer } from '../actions/drawerActions'

// Icons
import DashboardIcon from 'material-ui/svg-icons/action/dashboard'
import SettingsIcon from 'material-ui/svg-icons/action/settings'

const menuItems = [
  { name: 'Dashboard', link: 'dashboard', icon: <DashboardIcon /> },
  { name: 'Settings', link: 'settings', icon: <SettingsIcon /> }
]

export class SideMenu extends Component {
  render() {
    if (this.props.isMobile) {
      return (
        <Drawer
          docked={false}
          width={200}
          open={this.props.drawer}
          onRequestChange={() => this.props.ToggleDrawer()} >
          {
            menuItems.map((item) => {
              return (
                <ListItem
                  key={item.name}
                  containerElement={<Link to={`/${item.link}`} />}
                  primaryText={item.name}
                  leftIcon={item.icon}
                  onTouchTap={() => this.props.ToggleDrawer()}
                />
              )
            })
          }
        </Drawer>
      )
    } else {
      return (
        <div>
          <Paper
            style={this.props.drawer ? styles.menu.open : styles.menu.closed}
            zDepth={0}
            rounded={false} >
            <List>
              {
                menuItems.map((item) => {
                  return (
                    <ListItem
                      key={item.name}
                      containerElement={<Link to={`/${item.link}`} />}
                      primaryText={item.name}
                      leftIcon={item.icon}
                    />
                  )
                })
              }
            </List>
          </Paper>
        </div>
      )
    }
  }
}

SideMenu.propTypes = {
  drawer: PropTypes.bool,
  isMobile: PropTypes.bool,
  ToggleDrawer: PropTypes.func.isRequired,
}

const styles = {
  menu: {
   open: {
     width: '16em',
     height: '100%',
     order: -1,
     transition: 'margin 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
     marginLeft: 0,
     display: 'flex',
     flexDirection: 'column',
     justifyContent: 'space-between',
   },
   closed: {
     width: '16em',
     height: '100%',
     order: -1,
     transition: 'margin 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
     marginLeft: '-16em',
     display: 'flex',
     flexDirection: 'column',
     justifyContent: 'space-between',
   }
  }
}

export const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    ToggleDrawer: ToggleDrawer
  }, dispatch)
}

export const mapStateToProps = state => {
  return {
    drawer: state.drawer
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SideMenu)

