import React, { Component } from 'react'
import PropTypes from 'prop-types'
import FlatButton from 'material-ui/FlatButton'
import Snackbar from 'material-ui/Snackbar'

export class SocialLogin extends Component {
	constructor() {
		super()
		this.state = {
			signInError: false,
			loaded: true
		}

		this.loginWith = this.loginWith.bind(this)
		this.closeSnackBar = this.closeSnackBar.bind(this)
	}

	loginWith(providerId) {
		let self = this

		this.setState({
			loaded: false
		})

		this.props.SignInWithPopup(providerId)
			.then(() => {
				self.setState({
					signInError: false,
					loaded: true
				})
			})
			.catch(error => {
				self.setState({
					signInError: error,
					loaded: true
				})
			})
	}

	closeSnackBar() {
    this.setState({signInError: false})
  }

	render() {
		return(
			<div>
				<FlatButton
					id='btnFacebook'
					type='submit'
					label='Login with Facebook'
					primary={true}
					fullWidth={true}
					onClick={() => this.loginWith('facebook')}
				/>
				<FlatButton
					id='btnGoogle'
					type='submit'
					label='Login with Google'
					primary={true}
					fullWidth={true}
					onClick={() => this.loginWith('google')}
				/>
				{
					this.state.signInError &&
					<Snackbar
						open={true}
						autoHideDuration={4000}
						message={this.state.signInError.message}
						onRequestClose={this.closeSnackBar} />
				}
      </div>
		)
	}
}

SocialLogin.propTypes = {
	SignInWithPopup: PropTypes.func.isRequired
}

export default SocialLogin
