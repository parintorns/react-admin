import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import AppBar from 'material-ui/AppBar'
import Avatar from 'material-ui/Avatar'
import IconMenu from 'material-ui/IconMenu'
import IconButton from 'material-ui/IconButton'
import MenuItem from 'material-ui/MenuItem'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import { isEmpty } from 'underscore'
import { Link } from 'react-router-dom'

import {
  ToggleDrawer,
  SignOut
} from '../actions'

// Icons
import LogoutIcon from 'material-ui/svg-icons/action/power-settings-new'
import AccountIcon from 'material-ui/svg-icons/social/person'

export class TopMenu extends Component {

  constructor(props) {
    super(props)
    this.state = {
      openDialog: false
    }

    this.handleOpen = this.handleOpen.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.logout = this.logout.bind(this)
  }

  handleOpen() {
    this.setState({
      openDialog: true
    })
  }

  handleClose() {
    this.setState({
      openDialog: false
    })
  }

  async logout() {
    await this.setState({
      openDialog: false
    })

    this.props.SignOut()
  }

  render() {
    const { props } = this

    return (
      <div>
        <AppBar
          title="Title"
          onLeftIconButtonClick={() => this.props.ToggleDrawer()}
          zDepth={1}
          iconElementRight={
            <IconMenu
              iconButtonElement={
                <IconButton style={{padding: 0}}>
                  <Avatar
                    src={props.account.photoURL}>
                  </Avatar>
                </IconButton>
              }
              targetOrigin={{horizontal: 'right', vertical: 'top'}}
              anchorOrigin={{horizontal: 'right', vertical: 'top'}} >
              <MenuItem
                primaryText="Account"
                containerElement={<Link to="/account" />}
                leftIcon={<AccountIcon />}
              />
              <MenuItem
                primaryText="Sign out"
                onClick={this.handleOpen}
                leftIcon={<LogoutIcon />}
              />
            </IconMenu>
          }
        />

        <Dialog
          title="Logout"
          actions={[
            <FlatButton
              label="No"
              primary={true}
              onTouchTap={this.handleClose}
            />,
            <FlatButton
              label="Yes"
              primary={true}
              onTouchTap={this.logout}
            />,
          ]}
          modal={false}
          open={this.state.openDialog}
          onRequestClose={this.handleClose}
          contentStyle={{width: 300}}>
          Are you sure?
        </Dialog>
      </div>
    )
  }
}

TopMenu.propTypes = {
  account: PropTypes.object.isRequired,
  ToggleDrawer: PropTypes.func.isRequired,
  SignOut: PropTypes.func.isRequired
}

export const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    ToggleDrawer: ToggleDrawer,
    SignOut: SignOut
  }, dispatch)
}

export const mapStateToProps = (state) => {
  return {
    account: state.account
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TopMenu)
