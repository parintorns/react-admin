// Account
export const GET_USER_PROFILE = 'GET_USER_PROFILE'
export const UPDATE_PROFILE = 'UPDATE_PROFILE'
export const UPDATE_PROFILE_FAILED = 'UPDATE_PROFILE_FAILED'
export const CLEAR_PROFILE = 'CLEAR_PROFILE'

// Drawer
export const TOGGLE_DRAWER = 'TOGGLE_DRAWER'
