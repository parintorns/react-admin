import * as Firebase from "firebase";

// Initialize Firebase
const config = {
  apiKey: "AIzaSyDoHZbd9X1Ya4H4hSuoMSX5Q-EgRSVOeYI",
  authDomain: "admin-d94a8.firebaseapp.com",
  databaseURL: "https://admin-d94a8.firebaseio.com",
  storageBucket: "admin-d94a8.appspot.com",
  messagingSenderId: "31767561659"
};

Firebase.initializeApp(config);

const FirebaseAuth = Firebase.auth

export { Firebase, FirebaseAuth };
