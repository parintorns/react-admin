export const required = value => value ? undefined : "Required";
export const letter = value => /^[A-Z \'\-]+$/i.test(value) ? undefined : "Letter Only";
export const email = value => /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value) ? undefined : "Invalid email address";
