import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Field, reduxForm } from 'redux-form'
import { TextField } from 'redux-form-material-ui'
import { Card, CardTitle, CardText, CardActions } from 'material-ui/Card'
import Avatar from 'material-ui/Avatar'
import RaisedButton from 'material-ui/RaisedButton'
import Snackbar from 'material-ui/Snackbar'
import _ from 'underscore'

import {
  UpdateProfile,
  UpdateUserEmail
} from '../actions'
import { required, email, letter } from '../helpers/validate'

export class Account extends Component {

  constructor(props) {
    super(props)
    this.state = {
      updateError: false
    }
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  /* istanbul ignore next */
  handleSubmit(props) {
    const { displayName, email, photoURL } = props
    const { initialValues } = this.props
    const profile = {}

    // Update profile
    if (initialValues.displayName !== displayName) profile.displayName = displayName
    if (initialValues.photoURL !== photoURL) profile.photoURL = photoURL

    if (!_.isEmpty(profile)) {
      this.props.UpdateProfile(profile)
        .then(() => {
          this.setState({
            updateError: {
              message: 'Profile Update Successful'
            }
          })
        })
        .catch(error => {
          this.setState({
            updateError: {
              message: error.message
            }
          })
        })
    }

    // Update Email
    if (initialValues.email !== email) {
      this.props.UpdateUserEmail(email)
        .then(() => {
          this.setState({
            updateError: {
              message: 'Email Update Successful'
            }
          })
        })
        .catch(error => {
          this.setState({
            updateError: {
              message: error.message
            }
          })
        })
    }
  }

  render() {
    const { initialValues, handleSubmit, pristine, submitting } = this.props
    return (
      <div style={styles.centered}>
        <Card style={styles.card}>
          <form onSubmit={handleSubmit(this.handleSubmit)}>
            <CardTitle title="Account" />
            <CardText>
              <div style={styles.centered}>
                <Avatar
                  src={initialValues.photoURL}
                  size={98}
                />
              </div>
              <Field
                name="displayName"
                type='text'
                component={TextField}
                floatingLabelText="Username"
                fullWidth={true}
                validate={[required, letter]}
              />
              <Field
                name='email'
                type='text'
                component={TextField}
                floatingLabelText="Email"
                fullWidth={true}
                validate={[required, email]}
              />
            </CardText>
            <CardActions style={styles.right}>
              <RaisedButton
                type="submit"
                label="Update"
                primary={true}
                disabled={pristine || submitting}
              />
            </CardActions>
          </form>
        </Card>
        {
          this.state.updateError &&
          <Snackbar
            open={true}
            autoHideDuration={4000}
            message={this.state.updateError.message}
            onRequestClose={()=>{this.setState({updateError: false})}}
          />
        }
      </div>
    )
  }
}

Account.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  pristine: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  initialValues: PropTypes.object.isRequired,
  UpdateProfile: PropTypes.func.isRequired,
  UpdateUserEmail: PropTypes.func.isRequired
}

const styles = {
  card: {
    margin: '2em'
  },
  centered: {
    display: 'flex',
    justifyContent: 'center'
  },
  right: {
    display: 'flex',
    justifyContent: 'flex-end'
  }
}

export const mapStateToProps = state => ({
  initialValues: state.account
})

export const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    UpdateProfile: UpdateProfile,
    UpdateUserEmail: UpdateUserEmail
  }, dispatch)
}

const AccountForm = reduxForm({
  form: 'account'
})(Account)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountForm)
