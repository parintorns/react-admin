import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Field, reduxForm } from 'redux-form'
import { TextField } from 'redux-form-material-ui'
import { Card, CardActions } from 'material-ui/Card'
import Avatar from 'material-ui/Avatar'
import Snackbar from 'material-ui/Snackbar'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
import CircularProgress from 'material-ui/CircularProgress'
import LockIcon from 'material-ui/svg-icons/action/lock-outline'
import { Link } from 'react-router-dom'

import SocialLogin from '../components/SocialLogin'
import {
  SignInWithEmailAndPassword,
  SignInWithPopup
} from '../actions'

export class Login extends Component {

  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      signInError: false,
      loaded: true
    }

    this.login = this.login.bind(this)
    this.closeSnackBar = this.closeSnackBar.bind(this)
  }

  login(props) {
    const { email, password } = props
    let self = this

    this.setState({
      loaded: false
    })

    this.props.SignInWithEmailAndPassword(email, password)
      .then(() => {
        self.setState({
          signInError: false,
          loaded: true
        })
      })
      .catch(error => {
        self.setState({
          signInError: error,
          loaded: true
        })
      })
  }

  closeSnackBar() {
    this.setState({signInError: false})
  }

  render() {
    const { handleSubmit } = this.props

    return (
      <div style={styles.main}>
        <Card>
          <div style={styles.avatar}>
            <Avatar icon={<LockIcon />} size={60} />
          </div>

          {
            this.state.signInError &&
            <Snackbar
              open={true}
              autoHideDuration={4000}
              message={this.state.signInError.message}
              onRequestClose={this.closeSnackBar} />
          }

          <form onSubmit={handleSubmit(this.login)}>
            <div style={styles.form}>
              <div style={styles.input} >
                <Field
                  type="text"
                  name="email"
                  component={TextField}
                  floatingLabelText="Email"
                  onChange={(text) => this.setState({email: text.target.value})}
                />
              </div>
              <div style={styles.input}>
                <Field
                  type="password"
                  name="password"
                  component={TextField}
                  floatingLabelText="Password"
                  onChange={(text) => this.setState({password: text.target.value})}
                />
              </div>
            </div>
            {
              !this.state.loaded ?
              <div style={styles.centered}>
                <CircularProgress size={25} style={{
                  marginBottom: 15
                }}/>
              </div> :
              <CardActions>
                <RaisedButton
                  type="submit"
                  label="Login"
                  disabled={ !this.state.email || !this.state.password }
                  primary={true}
                  fullWidth={true}
                />

              </CardActions>
            }
          </form>
        </Card>
        <div style={{
          marginTop: '2em'
        }}>
          <FlatButton
            label="Register"
            primary={true}
            fullWidth={true}
            containerElement={<Link to="/register" />}
          />
          <FlatButton
            label="Forgot Password"
            primary={true}
            fullWidth={true}
            containerElement={<Link to="/reset-password" />}
          />
          <SocialLogin
            SignInWithPopup={this.props.SignInWithPopup}
          />
        </div>
      </div>
    )
  }
}

Login.propTypes = {
  email: PropTypes.string,
  password: PropTypes.string,
  signInError: PropTypes.object,
  loaded: PropTypes.bool,
  handleSubmit: PropTypes.func,
  SignInWithEmailAndPassword: PropTypes.func.isRequired,
  SignInWithPopup: PropTypes.func.isRequired
}

const styles = {
  main: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatar: {
    margin: '1em',
    textAlign: 'center',
  },
  form: {
    padding: '0 1em 1em 1em',
  },
  input: {
    display: 'flex',
  },
  centered: {
    textAlign: 'center'
  }
}

export const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    SignInWithEmailAndPassword: SignInWithEmailAndPassword,
    SignInWithPopup: SignInWithPopup
  }, dispatch)
}

const LoginForm = reduxForm({
  form: 'login'
})(Login)

export default connect(
  null,
  mapDispatchToProps
)(LoginForm)
