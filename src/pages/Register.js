import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Field, reduxForm } from 'redux-form'
import { TextField } from 'redux-form-material-ui'
import { Card, CardActions } from 'material-ui/Card'
import Snackbar from 'material-ui/Snackbar'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
import CircularProgress from 'material-ui/CircularProgress'
import { Link } from 'react-router-dom'

import {
  CreateUserWithEmailAndPassword,
  SignInWithPopup
} from '../actions'
import SocialLogin from '../components/SocialLogin'

export class Register extends Component {
	constructor(props) {
    super(props)
    this.state = {
      displayName: '',
      email: '',
      password:'',
      signUpError: false,
      loaded: true
    }

    this.handleLoginWithEmail = this.handleLoginWithEmail.bind(this)
    this.closeSnackBar = this.closeSnackBar.bind(this)
  }

  handleLoginWithEmail(props) {
    const self = this
		const { email, password, displayName } = props
    const userData = {
      displayName: displayName
    }

    this.setState({
      loaded: false
    })

    this.props.CreateUserWithEmailAndPassword(email, password, userData)
      .then((result) => {
        self.setState({
          signUpError: false,
          loaded: true
        })
      })
      .catch(error => {
        self.setState({
          signUpError: error,
          loaded: true
        })
      })
  }

  closeSnackBar() {
    this.setState({signUpError: false})
  }

	render() {
		const { handleSubmit } = this.props

		return (
			<div style={styles.main}>
        <Card>
          {
            this.state.signUpError &&
            <Snackbar
              open={true}
              autoHideDuration={4000}
              message={this.state.signUpError.message}
              onRequestClose={this.closeSnackBar} />
          }

          <form onSubmit={handleSubmit(this.handleLoginWithEmail)}>
            <div style={styles.form}>
              <div style={styles.input} >
                <Field
                  id='inputDisplayName'
                  type='text'
                  name='displayName'
                  component={TextField}
                  floatingLabelText='Username'
                  onChange={(text) => this.setState({displayName: text.target.value})}
                />
              </div>
              <div style={styles.input} >
                <Field
                  id='inputEmail'
                  type='text'
                  name='email'
                  value='email@mail.com'
                  component={TextField}
                  floatingLabelText='Email'
                  onChange={(text) => this.setState({email: text.target.value})}
                />
              </div>
              <div style={styles.input}>
                <Field
                  id='inputPassword'
                  type='password'
                  name='password'
                  component={TextField}
                  floatingLabelText='Password'
                  onChange={(text) => this.setState({password: text.target.value})}
                />
              </div>
            </div>
            {
              !this.state.loaded ?
              <div style={styles.centered}>
                <CircularProgress size={25} style={{
                  marginBottom: 15
                }}/>
              </div> :
              <CardActions>
                <RaisedButton
                  id='btnRegister'
                  type='submit'
                  label='Register'
                  disabled={ !this.state.email || !this.state.password }
                  primary={true}
                  fullWidth={true}
                />
              </CardActions>
            }
          </form>
        </Card>
        <div style={{
          marginTop: '2em'
        }}>
					<FlatButton
            id='btnLogin'
						label='Login'
						primary={true}
						fullWidth={true}
						containerElement={<Link to='/' />}
					/>
          <SocialLogin
            SignInWithPopup={this.props.SignInWithPopup}
          />
        </div>
      </div>
		)
	}
}

Register.propTypes = {
  displayName: PropTypes.string,
  email: PropTypes.string,
  password: PropTypes.string,
  signUpError: PropTypes.object,
  loaded: PropTypes.bool,
  handleSubmit: PropTypes.func,
  CreateUserWithEmailAndPassword: PropTypes.func.isRequired,
  SignInWithPopup: PropTypes.func.isRequired
}

const styles = {
  main: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatar: {
    margin: '1em',
    textAlign: 'center',
  },
  form: {
    padding: '0 1em 1em 1em',
  },
  input: {
    display: 'flex',
  },
  centered: {
    textAlign: 'center'
  }
}

export const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    CreateUserWithEmailAndPassword: CreateUserWithEmailAndPassword,
    SignInWithPopup: SignInWithPopup
  }, dispatch)
}

const RegisterForm = reduxForm({
  form: 'register'
})(Register)

export default connect(
  null,
  mapDispatchToProps
)(RegisterForm)
