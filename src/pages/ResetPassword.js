import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Field, reduxForm } from 'redux-form'
import { TextField } from 'redux-form-material-ui'
import { Card, CardActions } from 'material-ui/Card'
import Snackbar from 'material-ui/Snackbar'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
import { Link } from 'react-router-dom'
import CircularProgress from 'material-ui/CircularProgress'

import {
  SendPasswordResetEmail
} from '../actions'

export class ResetPassword extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      loaded: true,
      resetPasswordError: false
    }

    this.resetPassword = this.resetPassword.bind(this)
  }

  resetPassword(props) {
    const { email } = props
    let self = this
    this.setState({
      loaded: false
    })

    this.props.SendPasswordResetEmail(email)
      .then(() => {
        self.setState({
          resetPasswordError: {
            message: 'Please check your email'
          },
          loaded: true
        })
      })
      .catch(error => {
        self.setState({
          resetPasswordError: error,
          loaded: true
        })
      })
  }

  render() {
    const { handleSubmit } = this.props

    return (
      <div style={styles.main}>
        <Card>
          <form onSubmit={handleSubmit(this.resetPassword)}>
            <div style={styles.form}>
              <div style={styles.input}>
                <Field
                  id="inputEmail"
                  type="text"
                  name="email"
                  component={TextField}
                  floatingLabelText="Email"
                  onChange={(text) => this.setState({email: text.target.value})}
                />
              </div>
              {
                !this.state.loaded
                ?
                <div style={styles.centered}>
                  <CircularProgress
                    size={25}
                    style={{ marginBottom: 15 }}
                  />
                </div>
                :
                <CardActions>
                  <RaisedButton
                    id="btnSubmit"
                    type="submit"
                    label="Reset Password"
                    disabled={this.state.email === ''}
                    primary={true}
                    fullWidth={true}
                  />
                </CardActions>
              }
            </div>
          </form>
        </Card>
        <div style={{
          marginTop: '2em'
        }}>
          <FlatButton
            label="Login"
            primary={true}
            fullWidth={true}
            containerElement={<Link to="/" />}
          />
        </div>
        {
          this.state.resetPasswordError &&
          <Snackbar
            open={true}
            autoHideDuration={4000}
            message={this.state.resetPasswordError.message}
            onRequestClose={() => this.setState({resetPasswordError: false})} />
        }
      </div>
    )
  }
}

ResetPassword.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  SendPasswordResetEmail: PropTypes.func.isRequired
}

const styles = {
  main: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
    alignItems: 'center',
    justifyContent: 'center',
  },
  form: {
    padding: '0 1em 1em 1em',
  },
  input: {
    display: 'flex',
  },
  centered: {
    textAlign: 'center'
  }
}

export const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    SendPasswordResetEmail: SendPasswordResetEmail
  }, dispatch)
}

const ResetPasswordForm = reduxForm({
  form: 'resetPassword'
})(ResetPassword)

export default connect(
  null,
  mapDispatchToProps
)(ResetPasswordForm)
