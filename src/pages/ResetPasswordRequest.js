import React from 'react'
import PropTypes from 'prop-types'
import { Card, CardText, CardActions } from 'material-ui/Card'
import RaisedButton from 'material-ui/RaisedButton'

export const ResetPasswordRequest = () => (
	<div style={styles.main}>
    <Card>
    	<CardText>
				<p>Please check your email</p>
    	</CardText>
    	<CardActions>
    		<RaisedButton
          type="submit"
          label="OK"
          primary={true}
          fullWidth={true}
        />
    	</CardActions>
    </Card>
  </div>
)

const styles = {
  main: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
    alignItems: 'center',
    justifyContent: 'center',
  },
  form: {
    padding: '0 1em 1em 1em',
  },
  input: {
    display: 'flex',
  },
  centered: {
    textAlign: 'center'
  }
}

export default ResetPasswordRequest
