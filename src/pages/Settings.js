import React, { Component } from 'react';
import {Card, CardTitle, CardText} from 'material-ui/Card';

export class Settings extends Component {
  render() {
    return (
      <Card style={styles.card}>
        <CardTitle title="Settings" />
        <CardText>

        </CardText>
      </Card>
    )
  }
}

const styles = {
  card: {
    margin: '2em'
  }
}

export default Settings;
