import {
  GET_USER_PROFILE,
  UPDATE_PROFILE,
  CLEAR_PROFILE
} from '../config'

export default function reducer(state={}, action) {
  switch(action.type) {
    case GET_USER_PROFILE:
      return Object.assign({}, state, action.payload)

    case UPDATE_PROFILE:
      return Object.assign({}, state, action.payload)

    case CLEAR_PROFILE:
      return null

    default:
      return state
  }
}
