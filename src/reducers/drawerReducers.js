const closed = !(/iPhone|iPad|iPod|Android/i.test(navigator.userAgent));

export default function reducer(state=closed, action) {
  switch(action.type) {
    case 'TOGGLE_DRAWER':
      return !state;

    default:
      return state;
  }
}
