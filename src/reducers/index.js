import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form'

import account from './accountReducers';
import drawer from './drawerReducers';

const reducers = {
	account,
	drawer,
  form
}

export default combineReducers(reducers);
