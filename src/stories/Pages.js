import React from 'react'
import { storiesOf } from '@storybook/react'
import Account from '../pages/Account'
import Settings from '../pages/Settings'
import Dashboard from '../pages/Dashboard'

storiesOf('Pages')
  .add('Dashboard', () => (
    <Dashboard />
  ))

  .add('Settings', () => (
    <Settings />
  ))

  .add('Account', () => (
    <Account handleSubmit={() => {}}/>
  ))
