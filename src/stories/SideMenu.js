import React from 'react'
import { storiesOf } from '@storybook/react'

import SideMenu from '../components/SideMenu'

storiesOf('Side Menu')
  .add('Desktop Mode', () => (
    <SideMenu drawer={true} isMobile={false} />
  ))

  .add('Mobile Mode', () => (
    <SideMenu drawer={true} isMobile={true} />
  ))
