import React from 'react'
import { storiesOf } from '@storybook/react'

import { TopMenu } from '../components/TopMenu'

storiesOf('Top Menu', module)
  .add('with Avatar', () => (
    <TopMenu
      account={{ photoURL: 'http://i.pravatar.cc/48' }}
    />
  ))
