import React from 'react'
import { storiesOf } from '@storybook/react'
import { linkTo } from '@storybook/addon-links'
import StoryRouter from 'storybook-router'

import Login from '../pages/Login'
import Register from '../pages/Register'
import ResetPassword from '../pages/ResetPassword'
import ResetPasswordRequest from '../pages/ResetPasswordRequest'

storiesOf('User Pages')
  .addDecorator(StoryRouter({
    '/': linkTo('User Pages', 'Login'),
    '/register': linkTo('User Pages', 'Register'),
    '/reset-password': linkTo('User Pages', 'Forgot Password'),
  }))

  .add('Login', () => (
    <Login handleSubmit={() => {}}/>
  ))

  .add('Register', () => (
    <Register handleSubmit={() => {}}/>
  ))

  .add('Forgot Password', () => (
    <ResetPassword handleSubmit={() => {}}/>
  ))

  .add('Reset Password Request', () => (
    <ResetPasswordRequest />
  ))

