export default {
	main: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh'
  },
  body: {
    display: 'flex',
    flex: 1,
    backgroundColor: '#edecec',
    overflow: 'hidden',
  },
  content: {
    flex: 1,
  },
  loading: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
    alignItems: 'center',
    justifyContent: 'center'
  }
};

